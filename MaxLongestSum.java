import java.util.Scanner;

public class MaxLongestSum
{
    // Private State

    public static void main( String[] args )
    {
	Scanner sc = new Scanner( System.in );
        int n = sc.nextInt();
        int[] values = new int[n+1];
	values[0] = -1;
        for( int i = 1; i <= n; ++i )
	    {
		values[i] = sc.nextInt();
	    }
	int[] S = new int[n+1];
	int[] C = new int[n+1];
	
	for( int j = 1; j <= n; ++j )
	    {
		S[j] = 1;
		for( int k = 1; k < j; ++k )
		    {
			if( values[k] < values[j] && S[j] < S[k] + 1 )
			    {
				System.out.printf( "k: %d, j: %d%n", k, j );
				System.out.printf( "A[k]=%d, C[j]=%d%n", values[k], C[j] );
				
				S[j] = S[k] + 1;

				if( S[j] == S[j] - 1 )
				    C[j] = j - 1;
				else
				    C[j] = k;
			    }
		    }
	    }

	int sMax = 0;
	for( int i = 0; i <= n; i++ )
	    sMax = Math.max( sMax, S[i] );

	int max = 0;
	for( int i = n; i > 0; --i )
	    {
		if( S[i] == sMax )
		    {
			int j = C[i];
			int count = values[i];
			while( j > 0 )
			    {
				System.out.printf( "I:%dJ:%d%n", i, j );
				System.out.printf( "%d += %d%n", count + values[j], values[j] );
				count += values[j];
				j = C[j];
			    }
			max = Math.max( max, count );
		    }
	    }
	for( int i = 1; i <= n; ++i )
	    {
		System.out.printf( "S[%d] = %d%n", i, S[i] );
	    }
	for( int i = 1; i <= n; ++i )
		System.out.printf( "C[%d] = %d%n", i, C[i] );
	System.out.println( max );
    }
}
