/**
 * LongestIncreasingSubseqRecursive.java
 * @author Alex Cavaliere <arc6393@rit.edu>
 * @author Jon Schenk <jds8899@rit.edu>
 * @date 2016-10-03
 */

import java.util.Scanner;

public class LongestIncreasingSubseqRecursive
{
    // Private State
    
    public static void main( String[] args )
    {
	Scanner sc = new Scanner( System.in );
        int n = sc.nextInt();
        int[] values = new int[n+1];
	values[0] = -1;
        for( int i = 1; i <= n; ++i ) {
            values[i] = sc.nextInt();
        }

	int max = -1;
	for( int j = n; j >= 1; --j )
	    {
		max = Math.max( max, 1 + incrSubseqRecursive( j, values ) );
	    }
	System.out.println( max );
    }// end main

    public static int incrSubseqRecursive( int j, int[] A )
    {
	int count = 0;
	if( j == 1 ) // base case
	    return 0;
	for( int i = j - 1; i >= 1; --i )
	    {
		if( A[j] > A[i] )
		    {
			count = Math.max( count,  1 + incrSubseqRecursive( i, A ) );
		    }
	    }
	return count;
    }
} // end class
