import java.util.Random;

public class Input
{
    public static void main( String[] args )
    {
	// n a b seed
	if( args.length != 4 )
	    {
		System.out.println( "Usage: java Input n a b seed" );
		System.exit( 1 );
	    }

	int n = Integer.parseInt( args[0] );
	int a = Integer.parseInt( args[1] );
	int b = Integer.parseInt( args[2] );
	long seed = Long.parseLong( args[3] );

	
	Random random = new Random( seed );

	int count = 0;
	
	System.out.println( n );
	
	while( count < n )
	    {
		System.out.printf( "%d ", random.nextInt( ( b - a ) + 1 ) + a );
		++count;
	    }
	
    }
}
